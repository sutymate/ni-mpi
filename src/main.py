import numpy as np
from numpy import linalg as lg
import math


def load_matrix(filepath):
    """
    Reads matrix either from file in filename path or from standard input.
    :param filepath: path to matrix txt file
    :return: 2D np.array with floats of NxM dimensions
    """
    if filepath:
        f = open(filepath, "r")
        line = f.readline()
    else:
        line = input()

    line = line.strip()
    # n is number of rows
    n = line.count(";") + 1
    # m is number of columns
    m = math.ceil(line.find(";") / 2.0)
    matrix = np.empty(shape=(n, m), dtype=float)
    m_i = 0
    for row in line.split(";"):
        n_i = 0
        for element in row.split():
            matrix[m_i, n_i] = float(element)
            n_i += 1
        m_i += 1
    return matrix


def eig_test(matrix):
    """
    Calculate the (semi)indefiniteness of matrix using eigenvalues.
    :param matrix: 2D float np.array
    :return: string descriptor
    """
    vals = lg.eigvals(matrix)
    positive_cnt = 0
    negative_cnt = 0
    zeroes = 0
    for val in vals:
        # float arithmetics
        if math.isclose(0.0, val):
            zeroes += 1
            # do not count the same number twice
            continue
        if val < 0:
            negative_cnt += 1
        elif val > 0:
            positive_cnt += 1
    n, _ = matrix.shape
    # all eigenvalues are positive bigger than zero
    if positive_cnt == n:
        return "pos"
    # all eigenvalues are negative smaller than zero
    if negative_cnt == n:
        return "neg"
    # all eigenvalues are positive or zero
    if positive_cnt + zeroes == n:
        return "semi pos"
    # all eigenvalues are negative or zero
    if negative_cnt + zeroes == n:
        return "semi neg"
    # both negative and positive eigenvalues -> indefiniteness
    return "indef"

def analyze(matrix):
    """
    Analyzes matrix using Sylvester criterion for definitiveness. If matrix is not definitive, test using eigenvalues.
    :param matrix: 2D np.array of float values
    :return: string message that tells (semi) definitivness of matrix
    """
    n, m = matrix.shape
    dets = []
    # calculating the determinant of sub matrices M_1 .. M_n
    for sub_i in range(1, n + 1):
        sub = matrix[0:sub_i, 0:sub_i]
        dets.append(lg.det(sub))
    positive_flag = True
    negative_flag = True
    semi_positive_flag = False
    semi_negative_flag = False
    indef_flag = False
    negative_cnt = 0
    k = 1
    for det in dets:
        # if any determinant is negative, matrix cannot be Positive definitive
        if det < 0:
            positive_flag = False
            # if determinant is negative for each M_k where k is uneven, matrix is Negative definitive
            if k % 2 == 1:
                negative_cnt += 1
            else:
                # if determinant is negative when k is even, matrix is not Negative definitive
                negative_cnt = float("-inf")
        k += 1

    if negative_cnt != n / 2:
        negative_flag = False
    method_description = "\nSolved by Sylvester criterion,"
    method_description += "\ndeterminants of submatrices m_1 .. m_n are:\n" + str(dets)

    # already found solution using easier method, dont calculate eigenvalues
    if not positive_flag and not negative_flag:
        answer = eig_test(matrix)
        if answer == "semi pos":
            semi_positive_flag = True
        elif answer == "semi neg":
            semi_negative_flag = True
        elif answer == "indef":
            indef_flag = True
        elif answer == "pos":
            positive_flag = True
        elif answer == "neg":
            negative_flag = True
        method_description = "\nSolved by decomposition of eigenvalues."


    message = "Matrix is neither positive nor negative definitive,"

    if positive_flag:
        message = "Positive definitive matrix."

    if negative_flag:
        message = "Negative definitive matrix."

    if semi_positive_flag:
        message = "Semi-positive definitive matrix."

    if semi_negative_flag:
        message = "Semi-negative definitive matrix."

    if indef_flag:
        message = "Indefinitive matrix."

    # method_description tells what method was used
    message += method_description

    return message


def test(filepath):
    matrix = load_matrix(filepath)
    n, m = matrix.shape
    if n != m:
        print("Irregular matrix.")
    else:
        answer = analyze(matrix)
        print(matrix)
        print(answer)


if __name__ == '__main__':
    filepaths = []
    # add test files for file input
    for i in range(1, 4):
        filepath = "../data/m" + str(i) + ".txt"
        filepaths.append(filepath)
    # add test for std input
    # filepaths.append(None)
    # test all
    for f in filepaths:
        test(f)
